
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>JSP Page</title>
    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</head>


<body>


<div class="container">


    <div class="row">
        <div class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span
                            class="icon-bar"></span><span class="icon-bar"></span>
                    </button>
                </div>

                <div class="collapse navbar-collapse">

                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="home">
                                Menu
                            </a>
                        </li>
                    </ul>
                </div>

            </div>
        </div>
    </div>


    <!-- ADD USER -->

    <div class="row">
        <div class="panel panel-success">
            <div class="panel-heading"><h1>Dodaj uzytkownika:</h1></div>
            <div class="panel-body">
                <div class="container" style="font-size: 18px">
                    <div class="col-md-12">

                        <form action = "/event_manager/add_user" method = "POST">
                            <table>
                                <tr>
                                    <th>Login</th><th><input type = "text" name = "login" placeholder="podaj login"></th>
                                </tr>
                                <tr>
                                    <th>Haslo</th><th><input type = "text" name = "password" placeholder="podaj haslo"/></th>
                                </tr>
                                <tr>
                                    <th>E-mail</th><th><input type = "text" name = "email" placeholder="podaj email"/></th>
                                </tr>
                                <tr>
                                    <th>Imie</th><th><input type = "text" name = "name" placeholder="podaj imie"/></th>
                                </tr>
                                <tr>
                                    <th><input type = "submit" value = "Submit"/></th>
                                </tr>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>

</body>
</html>