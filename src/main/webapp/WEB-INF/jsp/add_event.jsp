<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>add_event</title>
    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">

    <div class="row">
        <div class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span
                            class="icon-bar"></span><span class="icon-bar"></span>
                    </button>
                </div>


                <div class="collapse navbar-collapse">

                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="home">
                                Menu
                            </a>
                        </li>
                        <li>
                            <a href="add_user">
                               Add user
                            </a>
                        </li>

                    </ul>
                </div>

            </div>


        </div>
    </div>


<div class="row">
    <div class="panel panel-success">
        <div class="panel-heading"><h1>Dodaj wydarzenie:</h1></div>
        <div class="panel-body">
            <div class="container" style="font-size: 18px">
                <div class="col-md-12">

                    <form method="post" action="/event_manager/add_event">
                        <div class="name">
                            <div class="panel-heading"><h1>Nazwa wydarzenia:</h1></div>
                            <div class="panel-body">

                                <input type="text" name="name">

                            </div>
                        </div>
                        <div class="place">
                            <div class="panel-heading"><h1>Miejsce wydarzenia:</h1></div>
                            <div class="panel-body">
                                <input type="text" name="place">
                            </div>
                        </div>
                        <div class="date&time">

                        </div>
                        <div class="panel-heading"><h1>Data i godzina rozpoczecia wydarzenia:</h1></div>
                        <div class="panel-body">
                            <input type="datetime-local" name="date">
                        </div>
                        <input type="submit" value="Submit">
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
</div>

</body>
</html>