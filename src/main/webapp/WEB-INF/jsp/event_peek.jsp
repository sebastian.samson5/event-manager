<%@ page import="com.example.eventmanager.service.EventService" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>peek_event</title>
    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container" style="font-size: 18px">
        <div class="name">
            <div class="panel-heading"><h1>Nazwa wydarzenia:</h1></div>
            ${event.name}
            <div class="panel-body">

            </div>
        </div>
        <div class="place">
            <div class="panel-heading"><h1>Miejsce wydarzenia:</h1></div>
            ${event.address}

            <div class="panel-body">

            </div>
        </div>
        <div class="date&time">

        </div>
        <div class="panel-heading"><h1>Data i godzina rozpoczecia wydarzenia:</h1></div>
    ${event.date}

    <div class="panel-body">

        </div>

</div>
</body>
</html>