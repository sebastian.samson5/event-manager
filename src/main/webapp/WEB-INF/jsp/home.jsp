<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>JSP Page</title>
    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</head>


<body>


<div class="container">


    <div class="row">
        <div class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span
                            class="icon-bar"></span><span class="icon-bar"></span>
                    </button>
                </div>


                <div class="collapse navbar-collapse">

                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href='<c:url value="/add_user" />'>
                                Add user
                            </a>
                        </li>
                        <li>
                            <a href='<c:url value="/home" />'>
                                Menu
                            </a>
                        </li>

                    </ul>
                </div>

            </div>


        </div>
    </div>

    <h1>Hello Sebek</h1>

    <div class="row">
        <div class="panel panel-success">
            <div class="panel-heading"><h1>Moje wydarzenia:</h1>
                <button onclick="window.location.href = 'http://localhost:8080/event_manager/add_event'" type="button">
                    +Create
                </button>
            </div>
            <div class="panel-body">
                <div class="container" style="font-size: 18px">
                    <div class="col-md-12">
                        <c:if test="${fn:length(events) lt 1}">
                            <div class="alert alert-warning">
                                Brak eventów
                            </div>
                        </c:if>
                        <c:if test="${fn:length(events) gt 0}">
                            <table class="table table-striped table-hover ">
                                <thead>
                                <th class="text-info">#</th>
                                <th class="text-info">Nazwa wydarzenia:</th>
                                <th class="text-info">Adres:</th>
                                <th class="text-info">Data:</th>
                                <th class="text-info">Założyciel:</th>
                                </thead>
                                <tbody>
                                <c:forEach items="${events}" var="event" varStatus="loop">
                                    <tr>
                                        <td class="text-success">${loop.index + 1}</td>
                                        <td>
                                                <a href="/event_manager/event_peek?eventId=${event.eventId}">
                                                ${event.name}
                                                </a>
                                        </td>
                                        <td>${event.address}</td>
                                        <td>${event.date}</td>
                                        <td>${event.owner.name}</td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                        </c:if>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Uczestniczę w: -->

    <div class="row">
        <div class="panel panel-success">
            <div class="panel-heading"><h1>Uczestniczę w wydarzeniach:</h1></div>
            <div class="panel-body">
                <div class="container" style="font-size: 18px">
                    <div class="col-md-12">
                        <c:if test="${fn:length(events) lt 1}">
                            <div class="alert alert-warning">
                                Brak wydarzeń:
                            </div>
                        </c:if>
                        <c:if test="${fn:length(events) gt 0}">
                            <table class="table table-striped table-hover ">
                                <thead>
                                <th class="text-info">#</th>
                                <th class="text-info">Nazwa wydarzenia:</th>
                                <th class="text-info">Data:</th>
                                <th class="text-info">Stan:</th>
                                </thead>
                                <tbody>
                                <c:forEach items="${participations}" var="participation" varStatus="loop">
                                    <tr>
                                        <td class="text-success">${loop.index + 1}</td>
                                        <td>${participation.event.name}</td>
                                        <td>${participation.event.date}</td>
                                        <td>${participation.participation}</td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                        </c:if>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Zaproszenia -->
    <div class="row">
        <div class="panel panel-success">
            <div class="panel-heading"><h1>Zaproszenia:</h1></div>
            <div class="panel-body">
                <div class="container" style="font-size: 18px">
                    <div class="col-md-12">
                        <c:if test="${fn:length(events) lt 1}">
                            <div class="alert alert-warning">
                                Brak zaproszeń:
                            </div>
                        </c:if>
                        <c:if test="${fn:length(events) gt 0}">
                            <table class="table table-striped table-hover ">
                                <thead>
                                <th class="text-info">#</th>
                                <th class="text-info">Nazwa wydarzenia:</th>
                                <th class="text-info">Zapraszający:</th>
                                <th class="text-info">Data:</th>
                                </thead>
                                <tbody>
                                <c:forEach items="${invitations}" var="invitation" varStatus="loop">
                                    <tr>
                                        <td class="text-success">${loop.index + 1}</td>
                                        <td>${invitation.event.name}</td>
                                        <td>${invitation.fromUser.name}</td>
                                        <td>${invitation.event.data}</td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                        </c:if>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>