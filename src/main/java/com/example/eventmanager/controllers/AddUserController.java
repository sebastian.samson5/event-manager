package com.example.eventmanager.controllers;


import com.example.eventmanager.model.User;
import com.example.eventmanager.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
public class AddUserController {

    private UserService userService;

    public AddUserController(UserService userService) {
        this.userService = userService;
    }


    @GetMapping({"/add_user"})
    public String add_user(Model model, @RequestParam(value = "name", required = false, defaultValue = "") String name) {
        model.addAttribute("name", name);
        return "add_user";
    }

    @PostMapping({"/add_user"})
    public String add_user(AddUserForm addUserForm) {
        User user = new User();
        user.setLogin(addUserForm.getLogin());
        user.setPassword(addUserForm.getPassword());
        user.setEmail(addUserForm.getEmail());
        user.setName(addUserForm.getName());
        userService.add(user);
        return "redirect:/home";
    }


}



