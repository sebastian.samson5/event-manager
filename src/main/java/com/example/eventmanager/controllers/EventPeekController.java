package com.example.eventmanager.controllers;

import com.example.eventmanager.dao.EventDao;
import com.example.eventmanager.model.Event;
import com.example.eventmanager.service.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class EventPeekController {
    @Autowired
    private EventService eventService;

    @GetMapping("/event_peek")
    public ModelAndView test(@RequestParam("eventId") Long id) {
        Event event = eventService.findById(id);
        ModelAndView modelAndView = new ModelAndView("event_peek");
        modelAndView.addObject("event", event);
        return modelAndView;
    }
}
