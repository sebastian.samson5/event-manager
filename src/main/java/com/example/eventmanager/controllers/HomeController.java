package com.example.eventmanager.controllers;

import com.example.eventmanager.dao.InvitationDao;
import com.example.eventmanager.dao.ParticipationDao;
import com.example.eventmanager.model.User;
import com.example.eventmanager.service.EventService;
import com.example.eventmanager.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/")
public class HomeController {

    /*
     *   Class created by:  greg - 10:51  28.07.19
     */

    // ===========================================================
    // Constants
    // ===========================================================


    // ===========================================================
    // Fields
    // ===========================================================

    @Autowired
    @Qualifier("EventService")
    private EventService eventService;

    @Autowired
    @Qualifier("UserService")
    private UserService userService;

    @Autowired
    @Qualifier("ParticipationDaoImp")
    private ParticipationDao participationDao;


    @Autowired
    @Qualifier("InvitationDaoImp")
    private InvitationDao invitationDao;

    // ===========================================================
    // Constructors
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================


    // ===========================================================
    // Methods
    // ===========================================================





    @GetMapping("/home")
    public ModelAndView eventsView() {
        ModelAndView modelAndView = new ModelAndView("home");

        Long userId = 1L;
        User user = this.userService.getUserById(userId);

        modelAndView.addObject("user", user);
        modelAndView.addObject("events", this.eventService.findAllEvents());
        modelAndView.addObject("participations", this.userService.getUserParticipationList(user));
        modelAndView.addObject("invitations", this.userService.getUserInvitationList(user));

        return modelAndView;
    }


    // ===========================================================
    // Getter & Setter
    // ===========================================================


    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================


}
