package com.example.eventmanager.controllers;

import com.example.eventmanager.model.Event;
import com.example.eventmanager.service.EventService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

//import java.time.LocalDateTime;

@Controller
public class AddEventController {
    private EventService eventService;

    public AddEventController(EventService eventService) {
        this.eventService = eventService;
    }

    @GetMapping({"/add_event"})
    public String hello() {
        return "add_event";
    }

    @PostMapping({"/add_event"})
    public String saveEvent(AddEventForm addEventForm) {
        Event event = new Event();
        event.setAddress(addEventForm.getPlace());
        event.setName(addEventForm.getName());
        event.setDate(addEventForm.getDate());
        eventService.addEvent(event);
        return "redirect:/home";
    }


}
