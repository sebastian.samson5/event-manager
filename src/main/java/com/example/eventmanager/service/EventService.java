package com.example.eventmanager.service;

import com.example.eventmanager.dao.CommentDao;
import com.example.eventmanager.dao.EventDao;
import com.example.eventmanager.dao.ParticipationDao;
import com.example.eventmanager.model.Comment;
import com.example.eventmanager.model.Event;
import com.example.eventmanager.model.Participation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Qualifier("EventService")
public class EventService {

    /*
     *   Class created by:  greg - 09:26  28.07.19
     */

    // ===========================================================
    // Constants
    // ===========================================================


    // ===========================================================
    // Fields
    // ===========================================================

    @Autowired
    @Qualifier("EventDaoImp")
    private EventDao eventDao;

    @Autowired
    @Qualifier("ParticipationDaoImp")
    private ParticipationDao participationDao;

    @Autowired
    @Qualifier("CommentDaoImp")
    private CommentDao commentDao;


    // ===========================================================
    // Constructors
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================


    // ===========================================================
    // Methods
    // ===========================================================

    public void addEvent(Event event) {
        this.eventDao.add(event);
    }

    public List<Event> findAllEvents() {
        return this.eventDao.findAll();
    }

    public Event findById(Long id){return eventDao.findById(id);}

    public void removeEvent(Event event) {
        this.eventDao.delete(event);
    }

    public void addParticipation(Participation participation) {
        this.participationDao.add(participation);
    }

    public List<Participation> findAllParticipations(Event event) {
        return this.eventDao.findAllParticipations(event);
    }


    public void removeParticipation(Participation participation) {
        this.participationDao.delete(participation);
    }

    public void addComment(Comment comment) {
        this.commentDao.add(comment);
    }

    public List<Comment> findAllComments(Event event) {
        return this.eventDao.findAllComments(event);
    }

    public void removeComment(Comment comment) {
        this.commentDao.delete(comment);
    }


    // ===========================================================
    // Getter & Setter
    // ===========================================================


    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================


}
