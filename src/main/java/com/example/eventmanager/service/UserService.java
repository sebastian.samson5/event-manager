package com.example.eventmanager.service;

import com.example.eventmanager.dao.UserDao;
import com.example.eventmanager.model.Event;
import com.example.eventmanager.model.Invitation;
import com.example.eventmanager.model.Participation;
import com.example.eventmanager.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Qualifier("UserService")
public class UserService {

    /*
     *   Class created by:  greg - 14:54  28.07.19
     */

    // ===========================================================
    // Constants
    // ===========================================================


    // ===========================================================
    // Fields
    // ===========================================================

    @Autowired
    @Qualifier("UserDaoImp")
    private UserDao userDao;

    // ===========================================================
    // Constructors
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================


    // ===========================================================
    // Methods
    // ===========================================================


    public User getUserById(Long id) {
        return this.userDao.findById(id);
    }

    public void add(User user){
        this.userDao.add(user);
    }

    public User findById(Long id){
        return this.userDao.findById(id);
    }

    public List<User> findAll(){
        return this.userDao.findAll();
    }

    public void update(User user){
        this.userDao.update(user);
    }

    public void delete(User user){
        this.userDao.delete(user);
    }

    public List<Event> getUserEventList(User user){
        return this.userDao.getUserEventList(user);
    }

    public List<Participation> getUserParticipationList(User user){
        return this.userDao.getUserParticipationList(user);
    }

    public List<Invitation> getUserInvitationList(User user){
        return this.userDao.getUserInvitationList(user);
    }

    // ===========================================================
    // Getter & Setter
    // ===========================================================


    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================


}
