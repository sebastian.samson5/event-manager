package com.example.eventmanager.dao;

import com.example.eventmanager.model.Event;
import com.example.eventmanager.model.Invitation;
import com.example.eventmanager.model.Participation;
import com.example.eventmanager.model.User;

import java.util.List;

public interface UserDao {
    /*
     *   Interface created by:  greg - 22:02  27.07.19
     */


    // ===========================================================
    // Constants
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================


    // ===========================================================
    // Methods
    // ===========================================================

    public void add(User user);

    public User findById(Long id);

    public List<User> findAll();

    public void update(User user);

    public void delete(User user);

    public List<Event> getUserEventList(User user);

    public List<Participation> getUserParticipationList(User user);

    public List<Invitation> getUserInvitationList(User user);


}
