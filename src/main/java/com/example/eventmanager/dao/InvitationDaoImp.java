package com.example.eventmanager.dao;

import com.example.eventmanager.model.Invitation;
import com.example.eventmanager.model.Participation;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Component
@Qualifier("InvitationDaoImp")
public class InvitationDaoImp implements InvitationDao {

    /*
     *   Class created by:  greg - 22:31  30.07.19
     */

    // ===========================================================
    // Constants
    // ===========================================================


    // ===========================================================
    // Fields
    // ===========================================================

    @PersistenceContext
    private EntityManager entityManager;

    // ===========================================================
    // Constructors
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    @Override
    public void add(Invitation invitation) {
        this.entityManager.persist(invitation);
    }

    @Override
    public Invitation findById(Long id) {
        String hql = " FROM Invitation I WHERE I.invitationId = :id";
        Query query = this.entityManager.createQuery(hql);
        query.setParameter("id", id);

        List<Invitation> participationList = query.getResultList();

        if (participationList == null) return null;
        else if (participationList.size() == 0) return null;
        else return participationList.get(0);
    }

    @Override
    public List<Invitation> findAll() {
        String hql = " FROM Invitation";
        Query query = this.entityManager.createQuery(hql);

        return query.getResultList();
    }

    @Override
    public void update(Invitation invitation) {
        this.entityManager.merge(invitation);
    }

    @Override
    public void delete(Invitation invitation) {
        this.entityManager.remove(invitation);
    }

    // ===========================================================
    // Methods
    // ===========================================================


    // ===========================================================
    // Getter & Setter
    // ===========================================================


    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================


}
