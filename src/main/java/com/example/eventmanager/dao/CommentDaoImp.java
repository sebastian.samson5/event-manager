package com.example.eventmanager.dao;

import com.example.eventmanager.model.Comment;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Component
@Qualifier("CommentDaoImp")
public class CommentDaoImp implements CommentDao {

    /*
     *   Class created by:  greg - 10:29  28.07.19
     */

    // ===========================================================
    // Constants
    // ===========================================================


    // ===========================================================
    // Fields
    // ===========================================================

    @PersistenceContext
    private EntityManager entityManager;

    // ===========================================================
    // Constructors
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    @Override
    public void add(Comment comment) {
        this.entityManager.persist(comment);
    }

    @Override
    public Comment findById(Long id) {
        String hql = " FROM Comment C WHERE C.commentId = :id";
        Query query = this.entityManager.createQuery(hql);
        query.setParameter("id", id);

        List<Comment> eventList = query.getResultList();

        if (eventList == null) return null;
        else if (eventList.size() == 0) return null;
        else return eventList.get(0);
    }

    @Override
    public List<Comment> findAll() {
        String hql = " FROM Comment";
        Query query = this.entityManager.createQuery(hql);

        List<Comment> commentList = query.getResultList();

        return commentList;
    }

    @Override
    public void update(Comment comment) {
        this.entityManager.merge(comment);
    }

    @Override
    public void delete(Comment comment) {
        this.entityManager.remove(comment);
    }

    // ===========================================================
    // Methods
    // ===========================================================


    // ===========================================================
    // Getter & Setter
    // ===========================================================


    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================


}
