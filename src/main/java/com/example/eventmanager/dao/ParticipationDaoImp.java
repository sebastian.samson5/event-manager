package com.example.eventmanager.dao;

import com.example.eventmanager.model.Participation;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Component
@Qualifier("ParticipationDaoImp")
public class ParticipationDaoImp implements ParticipationDao {


    /*
     *   Class created by:  greg - 10:34  28.07.19
     */

    // ===========================================================
    // Constants
    // ===========================================================


    // ===========================================================
    // Fields
    // ===========================================================

    @PersistenceContext
    private EntityManager entityManager;

    // ===========================================================
    // Constructors
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    @Override
    public void add(Participation participation) {
        this.entityManager.persist(participation);
    }

    @Override
    public Participation findById(Long id) {
        String hql = " FROM Participation P WHERE P.participationId = :id";
        Query query = this.entityManager.createQuery(hql);
        query.setParameter("id", id);

        List<Participation> participationList = query.getResultList();

        if (participationList == null) return null;
        else if (participationList.size() == 0) return null;
        else return participationList.get(0);
    }

    @Override
    public List<Participation> findAll() {
        String hql = " FROM Participation";
        Query query = this.entityManager.createQuery(hql);

        return query.getResultList();
    }

    @Override
    public void update(Participation participation) {
        this.entityManager.merge(participation);
    }

    @Override
    public void delete(Participation participation) {
        this.entityManager.remove(participation);
    }

    // ===========================================================
    // Methods
    // ===========================================================


    // ===========================================================
    // Getter & Setter
    // ===========================================================


    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================


}
