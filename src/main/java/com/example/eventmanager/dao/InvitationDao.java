package com.example.eventmanager.dao;

import com.example.eventmanager.model.Invitation;

import java.util.List;

public interface InvitationDao {
    /*
     *   Interface created by:  greg - 21:58  27.07.19
     */


    // ===========================================================
    // Constants
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================


    // ===========================================================
    // Methods
    // ===========================================================

    public void add(Invitation invitation);

    public Invitation findById(Long id);

    public List<Invitation> findAll();

    public void update(Invitation invitation);

    public void delete(Invitation invitation);

}
