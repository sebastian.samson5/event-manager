package com.example.eventmanager.dao;

import com.example.eventmanager.model.Comment;
import com.example.eventmanager.model.Event;
import com.example.eventmanager.model.Participation;
import com.example.eventmanager.model.User;

import java.util.List;

public interface EventDao {
    /*
     *   Interface created by:  greg - 21:57  27.07.19
     */


    // ===========================================================
    // Constants
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================


    // ===========================================================
    // Methods
    // ===========================================================

    public void add(Event event);

    public Event findById(Long id);

    public List<Event> findAll();

    public void update(Event event);

    public void delete(Event event);

    public List<Participation> findAllParticipations(Event event);

    public List<Comment> findAllComments(Event event);

    public boolean isUserParticipateInEvent(Event event, User user);
}
