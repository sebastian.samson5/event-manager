package com.example.eventmanager.dao;

import com.example.eventmanager.model.Event;
import com.example.eventmanager.model.Invitation;
import com.example.eventmanager.model.Participation;
import com.example.eventmanager.model.User;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Component
@Qualifier("UserDaoImp")
public class UserDaoImp implements UserDao {


    /*
     *   Class created by:  greg - 14:19  28.07.19
     */

    // ===========================================================
    // Constants
    // ===========================================================


    // ===========================================================
    // Fields
    // ===========================================================

    @PersistenceContext
    private EntityManager entityManager;

    // ===========================================================
    // Constructors
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    @Override
    @Transactional
    public void add(User user) {
        this.entityManager.persist(user);
    }

    @Override
    public User findById(Long id) {
        String hql = " FROM User U WHERE U.userId = :id";
        Query query = this.entityManager.createQuery(hql);
        query.setParameter("id", id);

        List<User> userList = query.getResultList();

        if (userList == null) return null;
        else if (userList.size() == 0) return null;
        else return userList.get(0);
    }

    @Override
    public List<User> findAll() {
        String hql = " FROM User U WHERE U.eventId = :id";
        Query query = this.entityManager.createQuery(hql);

        return query.getResultList();
    }

    @Override
    public void update(User user) {
        this.entityManager.merge(user);
    }

    @Override
    public void delete(User user) {
        this.entityManager.remove(user);
    }

    @Override
    public List<Event> getUserEventList(User user) {
        String hql = " FROM Event E WHERE E.owner = :user";
        Query query = this.entityManager.createQuery(hql);
        query.setParameter("user", user);

        return query.getResultList();
    }

    @Override
    public List<Participation> getUserParticipationList(User user) {
        String hql = " FROM Participation P WHERE P.user = :user";
        Query query = this.entityManager.createQuery(hql);
        query.setParameter("user", user);

        return query.getResultList();
    }

    @Override
    public List<Invitation> getUserInvitationList(User user) {
        String hql = " FROM Invitation I WHERE I.toUser = :user";
        Query query = this.entityManager.createQuery(hql);
        query.setParameter("user", user);

        return query.getResultList();
    }


    // ===========================================================
    // Methods
    // ===========================================================


    // ===========================================================
    // Getter & Setter
    // ===========================================================


    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================


}
