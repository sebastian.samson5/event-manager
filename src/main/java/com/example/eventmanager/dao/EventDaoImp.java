package com.example.eventmanager.dao;

import com.example.eventmanager.model.Comment;
import com.example.eventmanager.model.Event;
import com.example.eventmanager.model.Participation;
import com.example.eventmanager.model.User;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;

@Component
@Qualifier("EventDaoImp")
public class EventDaoImp implements EventDao {


    /*
     *   Class created by:  greg - 09:46  28.07.19
     */

    // ===========================================================
    // Constants
    // ===========================================================


    // ===========================================================
    // Fields
    // ===========================================================

    @PersistenceContext
    private EntityManager entityManager;

    // ===========================================================
    // Constructors
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

//    String hql = " FROM Event E WHERE E.owner = :owner";
//    Query query = this.entityManager.createQuery(hql);
//        query.setParameter("owner", user);
//
//        return query.getResultList();

    @Override
    @Transactional
    public void add(Event event) {
        this.entityManager.persist(event);
    }

    @Override
    @Transactional
    public Event findById(Long id) {
        String hql = " FROM Event E WHERE E.eventId = :id";
        Query query = this.entityManager.createQuery(hql);
        query.setParameter("id", id);

        List<Event> eventList = query.getResultList();

        if (eventList == null) return null;
        else if (eventList.size() == 0) return null;
        else return eventList.get(0);
    }

    @Override
    public List<Event> findAll() {
        String hql = " FROM Event";
        Query query = this.entityManager.createQuery(hql);

        return query.getResultList();
    }

    @Override
    public void update(Event event) {
        this.entityManager.merge(event);
    }

    @Override
    public void delete(Event event) {
        this.entityManager.remove(event);
    }

    @Override
    public List<Participation> findAllParticipations(Event event) {
        String hql = " FROM Participation P WHERE P.event = :event";
        Query query = this.entityManager.createQuery(hql);
        query.setParameter("event", event);

        return query.getResultList();
    }

    @Override
    public List<Comment> findAllComments(Event event) {
        String hql = " FROM Comment C WHERE C.event = :event";
        Query query = this.entityManager.createQuery(hql);
        query.setParameter("event", event);

        return query.getResultList();
    }

    @Override
    public boolean isUserParticipateInEvent(Event event, User user) {
        throw new NotImplementedException();
    }

    // ===========================================================
    // Methods
    // ===========================================================


    // ===========================================================
    // Getter & Setter
    // ===========================================================


    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================


}
