package com.example.eventmanager.dao;

import com.example.eventmanager.model.Participation;

import java.util.List;

public interface ParticipationDao {
    /*
     *   Interface created by:  greg - 21:59  27.07.19
     */


    // ===========================================================
    // Constants
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================


    // ===========================================================
    // Methods
    // ===========================================================

    public void add(Participation participation);

    public Participation findById(Long id);

    public List<Participation> findAll();

    public void update(Participation participation);

    public void delete(Participation participation);

}
