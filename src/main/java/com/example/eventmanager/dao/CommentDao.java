package com.example.eventmanager.dao;

import com.example.eventmanager.model.Comment;

import java.util.List;

public interface CommentDao {
    /*
     *   Interface created by:  greg - 21:45  27.07.19
     */


    // ===========================================================
    // Constants
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================


    // ===========================================================
    // Methods
    // ===========================================================

    public void add(Comment comment);

    public Comment findById(Long id);

    public List<Comment> findAll();

    public void update(Comment comment);

    public void delete(Comment comment);

}
