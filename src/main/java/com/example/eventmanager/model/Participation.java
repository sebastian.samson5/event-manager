package com.example.eventmanager.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
public class Participation {


    // ===========================================================
    // Constants
    // ===========================================================

    public static final int PARTICIPATION_NOT_CONFIRMED = 0;
    public static final int PARTICIPATION_CONFIRMED = 1;


    // ===========================================================
    // Fields
    // ===========================================================

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    private Long participationId;


    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "eventId")
    @Getter
    private Event event;


    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "userId")
    @Getter
    private User user;

    @Column(nullable = false)
    @Getter
    @Setter
    private int participation = PARTICIPATION_NOT_CONFIRMED;


    // ===========================================================
    // Constructors
    // ===========================================================

    public Participation() {
    }

    public Participation(Event event, User user, int participation) {
        this.event = event;
        this.user = user;
        this.participation = participation;
    }

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================


    // ===========================================================
    // Methods
    // ===========================================================

    // ===========================================================
    // Getter & Setter
    // ===========================================================


    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
}