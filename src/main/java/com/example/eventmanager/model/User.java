package com.example.eventmanager.model;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
public class User {


    // ===========================================================
    // Constants
    // ===========================================================

    public static int USER_LEVEL_NORMAL = 0;
    public static int USER_LEVEL_ADMIN = 1;


    // ===========================================================
    // Fields
    // ===========================================================

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long userId;

    @Column(nullable = false)
    private String login;

    @Column(nullable = false)
    private String password;

    @Column(nullable = false)
    private String email;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private int userLevel;


    // ===========================================================
    // Constructors
    // ===========================================================

    public User() {

    }

    public User(String login, String password, String email, String name, int userLevel) {
        this.login = login;
        this.password = password;
        this.email = email;
        this.name = name;
        this.userLevel = userLevel;
    }



    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================


    // ===========================================================
    // Methods
    // ===========================================================


    // ===========================================================
    // Getter & Setter
    // ===========================================================


    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================


    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getUserLevel() {
        return userLevel;
    }

    public void setUserLevel(int userLevel) {
        this.userLevel = userLevel;
    }
}