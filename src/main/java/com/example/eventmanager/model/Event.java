package com.example.eventmanager.model;

//import lombok.Getter;
//import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
public class Event {


    // ===========================================================
    // Constants
    // ===========================================================

    public static final int EVENT_ACCESS_PRIVATE = 0;
    public static final int EVENT_ACCESS_PUBLIC = 1;


    // ===========================================================
    // Fields
    // ===========================================================

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    
    private Long eventId;

    @Column(nullable = false)
    
    
    private String name;


    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "owner_id")
    
    private User owner;

    @Column(nullable = false)
    
    
    private LocalDateTime date;

    @Column(nullable = false)
    
    
    private String address;


    public static int getEventAccessPrivate() {
        return EVENT_ACCESS_PRIVATE;
    }

    public static int getEventAccessPublic() {
        return EVENT_ACCESS_PUBLIC;
    }

    public Long getEventId() {
        return eventId;
    }

    public void setEventId(Long eventId) {
        this.eventId = eventId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getAccess() {
        return access;
    }

    public void setAccess(int access) {
        this.access = access;
    }

    // Event type:
    // 0 - private
    // 1 - public
    @Column(nullable = false)
    
    
    private int access = EVENT_ACCESS_PUBLIC;


    // ===========================================================
    // Constructors
    // ===========================================================

    public Event() {

    }

    public Event(String name, User owner, LocalDateTime date, String address, int access) {
        this.name = name;
        this.owner = owner;
        this.date = date;
        this.address = address;
        this.access = access;
    }

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================


    // ===========================================================
    // Methods
    // ===========================================================


    // ===========================================================
    // Getter & Setter
    // ===========================================================


    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

}