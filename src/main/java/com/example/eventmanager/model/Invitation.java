package com.example.eventmanager.model;

import lombok.Getter;

import javax.persistence.*;

@Entity
public class Invitation {


    // ===========================================================
    // Constants
    // ===========================================================


    // ===========================================================
    // Fields
    // ===========================================================

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    private Long invitationId;


    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "from_user")
    @Getter
    private User fromUser;


    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "to_user")
    @Getter
    private User toUser;


    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "event_id")
    @Getter
    private Event event;

    @Getter
    private String message;



    // ===========================================================
    // Constructors
    // ===========================================================

    public Invitation() {

    }

    public Invitation(User fromUser, User toUser, Event event, String message) {
        this.fromUser = fromUser;
        this.toUser = toUser;
        this.event = event;
        this.message = message;
    }

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================


    // ===========================================================
    // Methods
    // ===========================================================


    // ===========================================================
    // Getter & Setter
    // ===========================================================


    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

}