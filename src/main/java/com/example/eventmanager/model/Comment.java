package com.example.eventmanager.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
public class Comment {


    // ===========================================================
    // Constants
    // ===========================================================


    // ===========================================================
    // Fields
    // ===========================================================

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    private Long comentId;


    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "eventId")
    @Getter
    private Event event;


    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "userId")
    @Getter
    private User user;


    @Getter
    @Setter
    private String message;

    @Column(nullable = false)
    @Getter
    @Setter
    private LocalDateTime date;

    // ===========================================================
    // Constructors
    // ===========================================================

    public Comment() {
    }

    public Comment(Event event, User user, String message, LocalDateTime date) {
        this.event = event;
        this.user = user;
        this.message = message;
        this.date = date;
    }


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================


    // ===========================================================
    // Methods
    // ===========================================================


    // ===========================================================
    // Getter & Setter
    // ===========================================================


    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
}